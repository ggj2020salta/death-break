extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var speed = 100
#const gravity = 9.81
const gravity = 0
var is_using_stairs
# Called when the node enters the scene tree for the first time.
func _ready():
	is_using_stairs	= false
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var movement = Vector2.ZERO
	if Input.is_action_pressed("ui_left"):
		movement.x += -1
	if Input.is_action_pressed("ui_right"):
		movement.x += 1
	pass
	if Input.is_action_just_pressed("ui_up"):
		set_collision_mask_bit(2,true)
		is_using_stairs = true
		
	if is_using_stairs and not Input.is_action_pressed("ui_up"):
		if get_floor_normal() == Vector2(0,-1):
			set_collision_mask_bit(2,false)
			pass
#		set_collision_mask_bit(2,false)
		pass
	movement.y = gravity
	move_and_slide_with_snap(movement*speed, Vector2(0,100), Vector2.UP)




